/*
 * motor_swerve.h
 *
 *  Created on: Jan 4, 2023
 *      Author: adamm
 */

#ifndef INC_MOTOR_SWERVE_H_
#define INC_MOTOR_SWERVE_H_

#include "main.h"
#include "stm32f407xx.h"
#include "stdlib.h"
#include "math.h"

/* Function In Program */
void pwm_motor_steer(int16_t pwm_steer[3]);
void pwm_motor_drive(int16_t pwm_drive[3]);
void control_posisi_motor_steer(float *steerPositionDeg, float *steerTargetDeg, int16_t *output);
void control_kecepatan_motor_drive(int16_t encSpeedMotorDrive[3], int16_t targetSpeedMotorDrive[3], int16_t *output);
void motor_vektor_kine(float *vx, float *vy, float *w, int16_t *targetDrive, float *targetSteer);


/* Variable In Program */


#endif /* INC_MOTOR_SWERVE_H_ */
