/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */
void steering_routine();

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define portPWMMotorDrive0_Pin GPIO_PIN_5
#define portPWMMotorDrive0_GPIO_Port GPIOE
#define portPWMMotorSteer1_Pin GPIO_PIN_6
#define portPWMMotorSteer1_GPIO_Port GPIOE
#define optoSteer2_Pin GPIO_PIN_1
#define optoSteer2_GPIO_Port GPIOC
#define optoSteer1_Pin GPIO_PIN_3
#define optoSteer1_GPIO_Port GPIOC
#define encAMotorDrive0_Pin GPIO_PIN_0
#define encAMotorDrive0_GPIO_Port GPIOA
#define encBMotorDrive0_Pin GPIO_PIN_1
#define encBMotorDrive0_GPIO_Port GPIOA
#define portDirBMotorDrive1_Pin GPIO_PIN_5
#define portDirBMotorDrive1_GPIO_Port GPIOA
#define portPWMMotorSteer2_Pin GPIO_PIN_6
#define portPWMMotorSteer2_GPIO_Port GPIOA
#define portPWMMotorDrive1_Pin GPIO_PIN_7
#define portPWMMotorDrive1_GPIO_Port GPIOA
#define portDirAMotorDrive2_Pin GPIO_PIN_4
#define portDirAMotorDrive2_GPIO_Port GPIOC
#define portDirBMotorDrive2_Pin GPIO_PIN_5
#define portDirBMotorDrive2_GPIO_Port GPIOC
#define portDirAMotorSteer2_Pin GPIO_PIN_0
#define portDirAMotorSteer2_GPIO_Port GPIOB
#define portDirBMotorSteer2_Pin GPIO_PIN_1
#define portDirBMotorSteer2_GPIO_Port GPIOB
#define portDirAMotorSteer1_Pin GPIO_PIN_2
#define portDirAMotorSteer1_GPIO_Port GPIOB
#define portDirBMotorSteer1_Pin GPIO_PIN_7
#define portDirBMotorSteer1_GPIO_Port GPIOE
#define encAMotorSteer1_Pin GPIO_PIN_9
#define encAMotorSteer1_GPIO_Port GPIOE
#define portDirAMotorDrive0_Pin GPIO_PIN_10
#define portDirAMotorDrive0_GPIO_Port GPIOE
#define encBMotorSteer1_Pin GPIO_PIN_11
#define encBMotorSteer1_GPIO_Port GPIOE
#define portDirBMotorDrive0_Pin GPIO_PIN_13
#define portDirBMotorDrive0_GPIO_Port GPIOE
#define portDirAMotorSteer0_Pin GPIO_PIN_14
#define portDirAMotorSteer0_GPIO_Port GPIOE
#define portDirBMotorSteer0_Pin GPIO_PIN_11
#define portDirBMotorSteer0_GPIO_Port GPIOB
#define portPWMMotorSteer0_Pin GPIO_PIN_14
#define portPWMMotorSteer0_GPIO_Port GPIOB
#define portDirAMotorDrive1_Pin GPIO_PIN_8
#define portDirAMotorDrive1_GPIO_Port GPIOD
#define encAMotorSteer2_Pin GPIO_PIN_6
#define encAMotorSteer2_GPIO_Port GPIOC
#define encBMotorSteer2_Pin GPIO_PIN_7
#define encBMotorSteer2_GPIO_Port GPIOC
#define optoSteer0_Pin GPIO_PIN_12
#define optoSteer0_GPIO_Port GPIOA
#define encAMotorSteer0_Pin GPIO_PIN_15
#define encAMotorSteer0_GPIO_Port GPIOA
#define encBMotorSteer0_Pin GPIO_PIN_3
#define encBMotorSteer0_GPIO_Port GPIOB
#define encAMotorDrive2_Pin GPIO_PIN_4
#define encAMotorDrive2_GPIO_Port GPIOB
#define encBMotorDrive2_Pin GPIO_PIN_5
#define encBMotorDrive2_GPIO_Port GPIOB
#define encAMotorDrive1_Pin GPIO_PIN_6
#define encAMotorDrive1_GPIO_Port GPIOB
#define encBMotorDrive1_Pin GPIO_PIN_7
#define encBMotorDrive1_GPIO_Port GPIOB
#define portPWMMotorDrive2_Pin GPIO_PIN_9
#define portPWMMotorDrive2_GPIO_Port GPIOB

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */
