/*
 * motor_swerve.c
 *
 *  Created on: Jan 4, 2023
 *      Author: adamm
 */

#include "motor_swerve.h"
float kpSteer[3] =
{ 8, 8, 8 }, kiSteer[3] =
{ 0, 0, 0 }, kdSteer[3] =
{ 0.1, 0.1, 0.1 };
float kpDrive[3] =
{ 2,2,2 }, kiDrive[3] =
{ 0 }, kdDrive[3] =
{ 0 };

float liat_target[3], liat_error[3], liat_pwm[3];

float nearestSteerDeg[3] = {0};

//void motor_vektor_kine(int16_t kecepatan_x, kecepatan_y, kecepatan_theta)
//{
//	int16_t buffer_x[3], buffer_y[3];
//	int16_t swerve_sudut[3], swerve_kecepatan[3];
//
//	buffer_x[0] = kecepatan_x + kecepatan_theta * cos(60 * 0.0174533);
////	buffer_x[1] = kecepatan_x
//}

void motor_vektor_kine(float *vx, float *vy, float *w, int16_t *targetDrive, float *targetSteer)
{
	float v_x_buffer[3] =
	{ *vx , *vx, *vx};
	float v_y_buffer[3] =
	{ *vy , *vy, *vy};
	float w_x_buffer[3] =
	{ 0 };
	float w_y_buffer[3] =
	{ 0 };

	w_x_buffer[0] = *w * 0.5;
	w_y_buffer[0] = *w * -0.86602540378;
	w_x_buffer[1] = *w * 0.5;
	w_y_buffer[1] = *w * 0.86602540378;
	w_x_buffer[2] = *w * -1;
	w_y_buffer[2] = *w * 0;

	for (int i = 0; i < 3; i++)
	{
		v_x_buffer[i] += w_x_buffer[i];
		v_y_buffer[i] += w_y_buffer[i];

		targetDrive[i] = sqrtf(v_x_buffer[i] * v_x_buffer[i] + v_y_buffer[i] * v_y_buffer[i]);
		targetSteer[i] = atan2f(v_y_buffer[i], v_x_buffer[i]) * 57.2958;

		liat_target[i] = targetSteer[i];

		if (targetSteer[i] < 0)
			targetSteer[i] += 360;
	}
}

void control_posisi_motor_steer(float *steerPositionDeg, float *steerTargetDeg, int16_t *output)
{
	static float error[3], prevError[3], realError[3];
	static float pSteer[3], iSteer[3], dSteer[3];
	static float outputPWMSteer[3];

	for (int i = 0; i < 3; i++)
	{
		prevError[i] = error[i];
		realError[i] = steerTargetDeg[i] - steerPositionDeg[i];


		if(realError[i] > 90)
		{
			nearestSteerDeg[i] = 1;
			error[i] = realError[i] - 180;
		}
		else if(realError[i] < -90)
		{
			nearestSteerDeg[i] = 1;
			error[i] = realError[i] + 180;
		}
		else
		{
			nearestSteerDeg[i] = 0;
			error[i] = realError[i];
		}

		if (error[i] < -180)
			error[i] += 360;
		else if (error[i] > 180)
			error[i] -= 360;

		liat_error[i] = error[i];

		pSteer[i] = kpSteer[i] * error[i];
		iSteer[i] += kiSteer[i] * error[i];
		dSteer[i] = kdSteer[i] * (error[i] - prevError[i]);

		iSteer[i] = fminf(499, fmaxf(-499, iSteer[i]));

		outputPWMSteer[i] = pSteer[i] + iSteer[i] + dSteer[i];

		outputPWMSteer[i] = fminf(499, fmaxf(-499, outputPWMSteer[i]));

		output[i] = outputPWMSteer[i];
		liat_pwm[i] = output[i];
	}
}

void pwm_motor_steer(int16_t pwm_steer[3])
{
	if (pwm_steer[0] > 0)
	{
		HAL_GPIO_WritePin(portDirAMotorSteer0_GPIO_Port, portDirAMotorSteer0_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(portDirBMotorSteer0_GPIO_Port, portDirBMotorSteer0_Pin, GPIO_PIN_RESET);
	}
	else if (pwm_steer[0] < 0)
	{
		HAL_GPIO_WritePin(portDirAMotorSteer0_GPIO_Port, portDirAMotorSteer0_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(portDirBMotorSteer0_GPIO_Port, portDirBMotorSteer0_Pin, GPIO_PIN_SET);
	}
	else if (pwm_steer[0] == 0)
	{
		HAL_GPIO_WritePin(portDirAMotorSteer0_GPIO_Port, portDirAMotorSteer0_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(portDirBMotorSteer0_GPIO_Port, portDirBMotorSteer0_Pin, GPIO_PIN_RESET);
	}

	if (pwm_steer[1] > 0)
	{
		HAL_GPIO_WritePin(portDirAMotorSteer1_GPIO_Port, portDirAMotorSteer1_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(portDirBMotorSteer1_GPIO_Port, portDirBMotorSteer1_Pin, GPIO_PIN_SET);
	}
	else if (pwm_steer[1] < 0)
	{
		HAL_GPIO_WritePin(portDirAMotorSteer1_GPIO_Port, portDirAMotorSteer1_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(portDirBMotorSteer1_GPIO_Port, portDirBMotorSteer1_Pin, GPIO_PIN_RESET);
	}
	else if (pwm_steer[1] == 0)
	{
		HAL_GPIO_WritePin(portDirAMotorSteer1_GPIO_Port, portDirAMotorSteer1_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(portDirBMotorSteer1_GPIO_Port, portDirBMotorSteer1_Pin, GPIO_PIN_RESET);
	}

	if (pwm_steer[2] > 0)
	{
		HAL_GPIO_WritePin(portDirAMotorSteer2_GPIO_Port, portDirAMotorSteer2_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(portDirBMotorSteer2_GPIO_Port, portDirBMotorSteer2_Pin, GPIO_PIN_RESET);
	}
	else if (pwm_steer[2] < 0)
	{
		HAL_GPIO_WritePin(portDirAMotorSteer2_GPIO_Port, portDirAMotorSteer2_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(portDirBMotorSteer2_GPIO_Port, portDirBMotorSteer2_Pin, GPIO_PIN_SET);
	}
	else if (pwm_steer[2] == 0)
	{
		HAL_GPIO_WritePin(portDirAMotorSteer2_GPIO_Port, portDirAMotorSteer2_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(portDirBMotorSteer2_GPIO_Port, portDirBMotorSteer2_Pin, GPIO_PIN_RESET);
	}

	TIM12->CCR1 = abs(pwm_steer[0]);
	TIM9->CCR2 = abs(pwm_steer[1]);
	TIM13->CCR1 = abs(pwm_steer[2]);

}

void control_kecepatan_motor_drive(int16_t encSpeedMotorDrive[3], int16_t targetSpeedMotorDrive[3], int16_t *output)
{
	static float error[3], prevError[3];
	static float pDrive[3], iDrive[3], dDrive[3];
	static float outputPWMDrive[3];

	for (int i = 0; i < 3; i++)
	{
		if(nearestSteerDeg[i]==1)
		{
			targetSpeedMotorDrive[i] = -targetSpeedMotorDrive[i];
		}
		prevError[i] = error[i];
		error[i] = targetSpeedMotorDrive[i] - encSpeedMotorDrive[i];

		pDrive[i] = kpDrive[i] * error[i];
		iDrive[i] += kiDrive[i] * error[i];
		dDrive[i] = kdDrive[i] * (error[i] - prevError[i]);

		iDrive[i] = fminf(499, fmaxf(-499, iDrive[i]));

		outputPWMDrive[i] = pDrive[i] + iDrive[i] + dDrive[i];

		outputPWMDrive[i] = fminf(499, fmaxf(-499, outputPWMDrive[i]));

		output[i] = outputPWMDrive[i];
	}
}

void pwm_motor_drive(int16_t pwm_drive[3])
{
	if (pwm_drive[0] > 0)
	{
		HAL_GPIO_WritePin(portDirAMotorDrive0_GPIO_Port, portDirAMotorDrive0_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(portDirBMotorDrive0_GPIO_Port, portDirBMotorDrive0_Pin, GPIO_PIN_RESET);
	}
	else if (pwm_drive[0] < 0)
	{
		HAL_GPIO_WritePin(portDirAMotorDrive0_GPIO_Port, portDirAMotorDrive0_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(portDirBMotorDrive0_GPIO_Port, portDirBMotorDrive0_Pin, GPIO_PIN_SET);
	}
	else if (pwm_drive[0] == 0)
	{
		HAL_GPIO_WritePin(portDirAMotorDrive0_GPIO_Port, portDirAMotorDrive0_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(portDirBMotorDrive0_GPIO_Port, portDirBMotorDrive0_Pin, GPIO_PIN_RESET);
	}

	if (pwm_drive[1] > 0)
	{
		HAL_GPIO_WritePin(portDirAMotorDrive1_GPIO_Port, portDirAMotorDrive1_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(portDirBMotorDrive1_GPIO_Port, portDirBMotorDrive1_Pin, GPIO_PIN_RESET);
	}
	else if (pwm_drive[1] < 0)
	{
		HAL_GPIO_WritePin(portDirAMotorDrive1_GPIO_Port, portDirAMotorDrive1_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(portDirBMotorDrive1_GPIO_Port, portDirBMotorDrive1_Pin, GPIO_PIN_SET);
	}
	else if (pwm_drive[1] == 0)
	{
		HAL_GPIO_WritePin(portDirAMotorDrive1_GPIO_Port, portDirAMotorDrive1_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(portDirBMotorDrive1_GPIO_Port, portDirBMotorDrive1_Pin, GPIO_PIN_RESET);
	}

	if (pwm_drive[2] > 0)
	{
		HAL_GPIO_WritePin(portDirAMotorDrive2_GPIO_Port, portDirAMotorDrive2_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(portDirBMotorDrive2_GPIO_Port, portDirBMotorDrive2_Pin, GPIO_PIN_SET);
	}
	else if (pwm_drive[2] < 0)
	{
		HAL_GPIO_WritePin(portDirAMotorDrive2_GPIO_Port, portDirAMotorDrive2_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(portDirBMotorDrive2_GPIO_Port, portDirBMotorDrive2_Pin, GPIO_PIN_RESET);
	}
	else if (pwm_drive[2] == 0)
	{
		HAL_GPIO_WritePin(portDirAMotorDrive2_GPIO_Port, portDirAMotorDrive2_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(portDirBMotorDrive2_GPIO_Port, portDirBMotorDrive2_Pin, GPIO_PIN_RESET);
	}

	TIM9->CCR1 = abs(pwm_drive[0]);
	TIM14->CCR1 = abs(pwm_drive[1]);
	TIM11->CCR1 = abs(pwm_drive[2]);

}
