/* USER CODE BEGIN Header */
/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 * @attention
 *
 * Copyright (c) 2023 STMicroelectronics.
 * All rights reserved.
 *
 * This software is licensed under terms that can be found in the LICENSE file
 * in the root directory of this software component.
 * If no LICENSE file comes with this software, it is provided AS-IS.
 *
 ******************************************************************************
 */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "dma.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "motor_swerve.h"
#include "string.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Variabel For Swerve Drive */
int16_t encMotorSteer[3];
int16_t encMotorDrive[3];

int16_t panduSteerSpeed[3];
int16_t panduSteerPosition[3], panduSteerPositionLast[3];
float panduSteerPositionDeg[3];
float targetSteerPositionDeg[3];
int16_t targetDriveSpeed[3];

float kecepatanX, kecepatanY, kecepatanW;

/* Variabel Joystick */
uint8_t terimaJoystick[9];
int16_t axisJoystick[4];
uint16_t buttonsJoystick;
extern DMA_HandleTypeDef hdma_usart2_rx;
uint8_t btnX, btnKotak, btnSegitiga, btnBulat, btnR1, btnR2, btnR3, btnL1, btnL2, btnL3, btnSelect, btnStart, btnAtas, btnKanan, btnBawah, btnKiri;

/* Variable For Opto Swerve */
int8_t optoSteer[3];

/* Variable Trying */
int16_t cobaPwmSteer[3];
int16_t cobaPwmDrive[3];

int16_t max_encoder[3];
uint8_t opto_now[3];
uint8_t opto_last[3];

/* Variable Transmit and Receive Data Master Slave */
extern DMA_HandleTypeDef hdma_usart1_rx;
extern DMA_HandleTypeDef hdma_usart3_rx;
uint8_t receiveMaster[31];
uint8_t sendMaster[31] = {'i','t','s'};

float globalSpeedOut[3];
float degree;
int test,test1,test2;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_TIM1_Init();
  MX_TIM2_Init();
  MX_TIM3_Init();
  MX_TIM4_Init();
  MX_TIM5_Init();
  MX_TIM8_Init();
  MX_TIM6_Init();
  MX_TIM7_Init();
  MX_TIM9_Init();
  MX_TIM12_Init();
  MX_TIM13_Init();
  MX_TIM14_Init();
  MX_TIM11_Init();
  MX_USART2_UART_Init();
  MX_USART1_UART_Init();
  MX_USART3_UART_Init();
  /* USER CODE BEGIN 2 */
	/* Initialization Timer For Encoder */
	HAL_TIM_Encoder_Start(&htim1, TIM_CHANNEL_ALL);	// Encoder Steer Motor 1
	HAL_TIM_Encoder_Start(&htim2, TIM_CHANNEL_ALL);	// Encoder Steer Motor 0
	HAL_TIM_Encoder_Start(&htim3, TIM_CHANNEL_ALL);	// Encoder Drive Motor 2
	HAL_TIM_Encoder_Start(&htim4, TIM_CHANNEL_ALL);	// Encoder Drive Motor 1
	HAL_TIM_Encoder_Start(&htim5, TIM_CHANNEL_ALL);	// Encoder Drive Motor 0
	HAL_TIM_Encoder_Start(&htim8, TIM_CHANNEL_ALL);	// Encoder Steer Motor 2

	/* Initialization TImer For Interupt */
	HAL_TIM_Base_Start_IT(&htim6);	// Timer Interupt 1000 Hz (1 ms)
	HAL_TIM_Base_Start_IT(&htim7);	// TImer Interupt 50 Hz (20 ms)

	/* Initialization Timer For PWM */
	HAL_TIM_PWM_Start(&htim9, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim9, TIM_CHANNEL_2);
	HAL_TIM_PWM_Start(&htim12, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim11, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim13, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim14, TIM_CHANNEL_1);

	HAL_UART_Receive_DMA(&huart2, (uint8_t*) terimaJoystick, 9);
//	HAL_UART_Transmit_DMA(&huart1, (uint8_t*) sendMaster, 31);
	HAL_UART_Receive_DMA(&huart1, (uint8_t*) receiveMaster, 31);

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
	while (1)
	{
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
//		for (int i = 0; i < 3; i++)
//			opto_last[i] = opto_now[i];
//		opto_now[0] = HAL_GPIO_ReadPin(optoSteer0_GPIO_Port, optoSteer0_Pin);
//		opto_now[1] = HAL_GPIO_ReadPin(optoSteer1_GPIO_Port, optoSteer1_Pin);
//		opto_now[2] = HAL_GPIO_ReadPin(optoSteer2_GPIO_Port, optoSteer2_Pin);
//
//		if (!opto_now[0] && opto_last[0])
//		{
//			max_encoder[0] = htim2.Instance->CNT;
//			htim2.Instance->CNT = 0;
//		}
//		if (!opto_now[1] && opto_last[1])
//		{
//			max_encoder[1] = htim1.Instance->CNT;
//			htim1.Instance->CNT = 0;
//		}
//		if (!opto_now[2] && opto_last[2])
//		{
//			max_encoder[2] = htim8.Instance->CNT;
//			htim8.Instance->CNT = 0;
//		}
	}
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLM = 8;
  RCC_OscInitStruct.PLL.PLLN = 168;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 4;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_5) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
}

void calibration_swerve()
{
}

void HAL_UART_RxHalfCpltCallback(UART_HandleTypeDef *huart)
{
	if(huart->Instance == USART1)
	{
		if ((receiveMaster[0] == 'i' && receiveMaster[1] == 't' && receiveMaster[2] == 's') == 0)
		{
			test++;
			HAL_DMA_Abort(&hdma_usart1_rx);
			HAL_UART_Receive_DMA(&huart1, (uint8_t*) receiveMaster, 31);
		}
	}
/*	if (huart->Instance == USART2)
	{
		if ((terimaJoystick[0] == 'i' && terimaJoystick[1] == 't' && terimaJoystick[2] == 's') == 0)
		{

			HAL_DMA_Abort(&hdma_usart2_rx);
			HAL_UART_Receive_DMA(&huart2, (uint8_t*) terimaJoystick, 9);
		}
	}*/
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	if(huart->Instance == USART1)
	{
		if ((receiveMaster[0] == 'i' && receiveMaster[1] == 't' && receiveMaster[2] == 's') == 1)
		{
			test1++;
			memcpy(globalSpeedOut, receiveMaster+3, 12);
			memcpy(&degree, receiveMaster+15, 4);
		}
	}
/*
	if (huart->Instance == USART2)
	{
		if ((terimaJoystick[0] == 'i' && terimaJoystick[1] == 't' && terimaJoystick[2] == 's') == 1)
		{
			buttonsJoystick = (terimaJoystick[4] & 0xff) + ((terimaJoystick[3] & 0xff) << 8);
			btnL2		= (buttonsJoystick >> 0) & 0b1;
			btnR2		= (buttonsJoystick >> 1) & 0b1;
			btnL1 		= (buttonsJoystick >> 2) & 0b1;
			btnR1 		= (buttonsJoystick >> 3) & 0b1;
			btnSegitiga = (buttonsJoystick >> 4) & 0b1;
			btnBulat	= (buttonsJoystick >> 5) & 0b1;
			btnX 		= (buttonsJoystick >> 6) & 0b1;
			btnKotak 	= (buttonsJoystick >> 7) & 0b1;
			btnSelect 	= (buttonsJoystick >> 8) & 0b1;
			btnL3 		= (buttonsJoystick >> 9) & 0b1;
			btnR3 		= (buttonsJoystick >> 10) & 0b1;
			btnStart 	= (buttonsJoystick >> 11) & 0b1;
			btnAtas 	= (buttonsJoystick >> 12) & 0b1;
			btnKanan 	= (buttonsJoystick >> 13) & 0b1;
			btnBawah 	= (buttonsJoystick >> 14) & 0b1;
			btnKiri 	= (buttonsJoystick >> 15) & 0b1;

			axisJoystick[0] = terimaJoystick[7] - 127;
			axisJoystick[1] = -1*(terimaJoystick[8] - 127);
			axisJoystick[2] = terimaJoystick[5] -127;
			axisJoystick[3] = -1*(terimaJoystick[6] - 127);

			kecepatanX = axisJoystick[0] * 0.5;
			kecepatanY = axisJoystick[1] * 0.5;
			kecepatanW = axisJoystick[2] * 0.5;

			if(btnR1 == 0)
			{
				kecepatanX = axisJoystick[0];
				kecepatanY = axisJoystick[1];
				kecepatanW = axisJoystick[2];
			}
			if(btnR2 == 0)
			{
				kecepatanX = axisJoystick[0]*2;
				kecepatanY = axisJoystick[1]*2;
				kecepatanW = axisJoystick[2]*2;
			}

		}
	}
*/

}

void HAL_UART_ErrorCallback(UART_HandleTypeDef *huart)
{
	HAL_UART_Receive_DMA(&huart1, (uint8_t*) receiveMaster, 31);
}

uint8_t status_steering = 100;
void steering_routine()
{
	static int16_t pwm_steer[3], pwm_drive[3];
	static uint8_t opto_now[3], opto_last[3];
	static float deg_in[3], deg_out[3];

	for (int i = 0; i < 3; i++)
		opto_last[i] = opto_now[i];
	opto_now[0] = HAL_GPIO_ReadPin(optoSteer0_GPIO_Port, optoSteer0_Pin);
	opto_now[1] = HAL_GPIO_ReadPin(optoSteer1_GPIO_Port, optoSteer1_Pin);
	opto_now[2] = HAL_GPIO_ReadPin(optoSteer2_GPIO_Port, optoSteer2_Pin);

	switch (status_steering)
	{
	case 0:
		motor_vektor_kine(&globalSpeedOut[0], &globalSpeedOut[1], &globalSpeedOut[2], targetDriveSpeed, targetSteerPositionDeg);

		control_posisi_motor_steer(panduSteerPositionDeg, targetSteerPositionDeg, pwm_steer);
		pwm_motor_steer(pwm_steer);
		control_kecepatan_motor_drive(encMotorDrive, targetDriveSpeed, pwm_drive);
		pwm_motor_drive(pwm_drive);
		break;

	case 100:
		for (int i = 0; i < 3; i++)
			pwm_steer[i] = 50;
		pwm_motor_steer(pwm_steer);
		status_steering = 101;
		break;

	case 101:
		for (int i = 0; i < 3; i++)
			if (!opto_now[i] && opto_last[i])
			{
				deg_in[i] = panduSteerPositionDeg[i];
				pwm_steer[i] = 0;
			}

		pwm_motor_steer(pwm_steer);

		if (pwm_steer[0] == 0 && pwm_steer[1] == 0 && pwm_steer[2] == 0)
		{
			for (int i = 0; i < 3; i++)
				pwm_steer[i] = 50;
			pwm_motor_steer(pwm_steer);
			status_steering = 102;
		}
		break;

	case 102:
		for (int i = 0; i < 3; i++)
			if (opto_now[i] && !opto_last[i])
			{
				deg_out[i] = panduSteerPositionDeg[i];
				pwm_steer[i] = 0;
			}

		pwm_motor_steer(pwm_steer);

		if (pwm_steer[0] == 0 && pwm_steer[1] == 0 && pwm_steer[2] == 0)
		{
			panduSteerPositionDeg[0] = 300 + 0.5 * (deg_out[0] - deg_in[0]);
			panduSteerPositionDeg[1] = 75 + 0.5 * (deg_out[1] - deg_in[1]);
			panduSteerPositionDeg[2] = 180 + 0.5 * (deg_out[2] - deg_in[2]);

			for (int i = 0; i < 3; i++)
				pwm_steer[i] = 0;
			pwm_motor_steer(pwm_steer);
			status_steering = 0;
		}
		break;
	}
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */
	__disable_irq();
	while (1)
	{
	}
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
